<?php

namespace AppBundle\Controller;


use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class GenusController extends Controller
{
    /**
     * @Route("/genus/{genusName}")
     * @param $genusName
     * @return Response
     */
    public function showAction($genusName)
    {

        return $this->render('genus/show.html.twig', [
            'name' => $genusName
        ]);

    }

    /**
     * @Route("genus/{genusName}/notes")
     * @Method("GET")
     */
    public function getNotesAction()
    {

    }
}